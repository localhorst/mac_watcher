#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
    Date of creation:           2023/02/26
    Date of last modification:  2023/02/26
"""

#   switch
switch_ip_addr = "10.0.0.2"
switch_snmp_community = "public"

# email
mail_server_domain = 'smtp.maildomain.dev'
mail_server_port = 587
mail_server_password = "__PW_DB__"
mail_from_address = "sender@domain.com"
mail_from_name = "MAC-Watcher"
mail_to_address = "dummy@example.com"
mail_to_name = "Joe Doe"

# trusted macs
trusted_mac_addresses = [
"00:EE:00:EE:40:EE", #Router
"00:EE:C2:EE:82:EE", #Smartphone
]



