# mac_watcher

Scrape the MAC table from HP ProCurve switch and altert if a untrusted MAC is found.


## Overview ##
Scrape all known MAC addresses via snmp from a HP ProCurve switch.
If a reported MAC is not i a white list, a alert is sent via email.


## Install ##

- `pip install mac-vendor-lookup`
- `cd /opt/`
- `git clone https://git.mosad.xyz/localhorst/mac_watcher.git`
- `cd /opt/mac_watcher/`
- Set the constants in `config.py`
- `chmod +x /opt/mac_watcher/mac_watcher.py` 
- `cp scripts/mac-watcher.service /etc/systemd/system/mac-watcher.service`
- `systemctl daemon-reload && systemctl enable --now mac-watcher.service`


